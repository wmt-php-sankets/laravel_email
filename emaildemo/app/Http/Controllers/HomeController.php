<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use app\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Mail\OrderShipped1234;
use Illuminate\Http\Request;
use Illuminate\Contracts\Translation\HasLocalePreference;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function sendmail()
    {
        $order =\App\Order::find(1);
        Mail::to(Auth::user()->email)->send(new OrderShipped1234($order));
        return redirect('/home');
    }

    public function ship()
    {
        $order =\App\Order::findOrFail(1);
        $order1 =\App\Order::findOrFail(2);
        $when = now()->addMinutes(5);
//        $order =\App\Order::find(1);
        // Ship order...
//        $order =\App\Order::findOrFail(1);
//        $invoice = App\Invoice::find(1);
        Mail::to(Auth::user()->email)
//            Mail::to(Auth::user()->email)->locale('es')
    //            ->cc($order1)
    //            ->bcc($order)
//        ->queue(new OrderShipped1234($order));
//        ->later($when, new OrderShipped1234($order));

            ->send(new OrderShipped1234($order)) ;

        return redirect('/home');
//        return (new OrderShipped1234($order));
    }

    public function preferredLocale()
    {
        return $this->locale;
    }
    public function sendEmail(){
        $hello =User::find(1);
        $job =(new \App\Jobs\SendEmailJob($hello))
            ->delay(\Carbon\Carbon::now()->addSecond(2));
       dispatch($job);
        echo "send successfull";
    }

}

//Mail::to($request->user())->locale('es')->send(
//    new OrderShipped($order));
