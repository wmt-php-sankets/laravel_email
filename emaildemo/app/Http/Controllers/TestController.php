<?php

namespace App\Http\Controllers;


use Faker\Provider\File;
use Illuminate\Http\Request;

use App\Http\Requests;

use Mail;

use App\Mail\MyDemoMail;


class TestController extends Controller

{


    /**

     * Send My Demo Mail Example

     *

     * @return void

     */

    public function myDemoMail()

    {
        $myEmail = 'aatmaninfotech@gmail.com';
        Mail::to($myEmail)->send(new MyDemoMail());
        return view('home');
    }


}

