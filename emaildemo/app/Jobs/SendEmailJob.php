<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 120;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $hello;
    public function __construct($hello)
    {
        $this->hello=$hello;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function retryUntil()
    {
        return now()->addSeconds(5);
    }

    public function handle()
    {
        \Mail::to('your_receiver_email@gmail.com')->send(new \App\Mail\SendEmailMailable($this->hello));
    }
    public function failed(Exception $exception)
    {
       echo "error are occurance";
    }
}
