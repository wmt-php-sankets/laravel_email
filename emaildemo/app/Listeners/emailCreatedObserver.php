<?php

namespace App\Listeners;

use App\Events\emailObserver;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class emailCreatedObserver
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  emailObserver  $event
     * @return void
     */
    public function handle(emailObserver $event)
    {
        //
    }
}
