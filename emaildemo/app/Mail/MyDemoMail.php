<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
class MyDemoMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

//        Storage::disk('public')->put('sample.txt', 'Contents');
        return $this->view('emails.myDemoMail')
            ->attach(public_path('sample.txt'), [
                'as' => 'sample.txt',
                'mime' => 'application/txt',
            ]);
//            /Desktop/demo
//              ->attachFromStorageDisk('s3', '/Desktop/demo/sample.txt');
//           ->attachData('hello there i am here!', 'sample.txt');
//             ->attachData($this->txt, 'sample.pdf', [
//        'mime' => 'application/pdf',
//    ]);

    }
//             ->attachFromStorage('public/sample.txt');
//        ->attachFromStorageDisk('s3', '/path/to/file');}
}
