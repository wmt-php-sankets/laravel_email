<?php

namespace App\Observers;

use App\User;

class emailObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {

        $order=User::find(1);

        \Mail::to('your_receiver_email@gmail.com')->send(new \App\Mail\MyTestMail($order));

//        $job =(new \App\Jobs\SendEmailJob($user))
//            ->delay(\Carbon\Carbon::now()->addSecond(2));
//           dispatch($job);
        echo "send successfull";
//        dd('sasa');
    }



    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        dd('data are saved in database');
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
