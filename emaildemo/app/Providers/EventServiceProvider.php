<?php

namespace App\Providers;

use App\Observers\emailObserver;
use App\User;
use App\Events\UserCreated;
use App\Listeners\UserCreatedListener;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
//        UserCreated::class=>[
//            UserCreatedListener::class
//        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
//        User::observe(UserObserver::class);
        User::observe(emailObserver::class);
    }
}
